<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class Attr extends Model
{
    public function products()
    {
        return $this->belongsToMany('App\Product', 'attr_product')->withTimestamps();
    }
}
