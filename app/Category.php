<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function products()
    {
        return $this->belongsToMany(Product::class)->withTimestamps();
    }

    public function related() {
        return Category::where('parent', $this->id)->get();
    }
}
