<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Attr;
use App\Category;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    if (!empty($query = $request->input('q'))) {
      $products = Product::where('title', 'LIKE', "%$query%")->paginate(4);
      //dd($products, $query);
    } else {
      $products = Product::paginate(4);
    }
    return view('admin.product.index', compact('products'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.product.create');
  }

  public function edit(Product $product)
  {
    dd('edit', $product);
  }

  /**
   * Display the specified resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function show(Product $product)
  {
    dd('show', $product);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $product = new Product;
    $product->title = $request->input('title');
    if (!empty($request->file('productImage'))) {
      $path = 'public/products';
      $id = $request->file('productImage')->store($path);
      $product->image = str_replace($path, '',  $id);
    }
    $product->description_1 = $request->input('description_1');
    $product->description_2 = $request->input('description_2');
    $product->description_3 = $request->input('description_3');
    $product->save();
    if (!empty($request->input('category'))) {
      $product->categories()->sync($request->input('category'));
    }
    $attrs = explode(',',str_replace(' ', '',  $request->input('attrs')));
    if (!empty($attrs[0])) {
      foreach($attrs as $attr) {
        $current_attr = Attr::where('name', '=' ,$attr)->first();
        if (!empty($current_attr)) {
          $attr_ids[] = $current_attr->id;
        } else {
          $new_attr = new Attr;
          $new_attr->name = $attr;
          $new_attr->save();
          $attr_ids[] = $new_attr->id;
        }
      }
      if (!empty($attr_ids)) {
        $product->attrs()->sync($attr_ids);
      }
    }
    if (empty($attrs[0])) {
      $product->attrs()->sync([]);
    }
    return redirect(route('product.index'));
  }

  public function update(Product $product, Request $request)
  {
    $product->title = $request->input('title');
    if (!empty($request->file('productImage'))) {
      $path = 'public/products';
      $id = $request->file('productImage')->store($path);
      $product->image = str_replace($path, '',  $id);
    }
    if (!empty($request->input('category'))) {
      $product->categories()->sync($request->input('category'));
    }
    $product->description_1 = $request->input('description_1');
    $product->description_2 = $request->input('description_2');
    $product->description_3 = $request->input('description_3');
    $attrs = explode(',',str_replace(' ', '',  $request->input('attrs')));
    if (!empty($attrs[0])) {
      foreach($attrs as $attr) {
        $current_attr = Attr::where('name', '=' ,$attr)->first();
        if (!empty($current_attr)) {
          $attr_ids[] = $current_attr->id;
        } else {
          $new_attr = new Attr;
          $new_attr->name = $attr;
          $new_attr->save();
          $attr_ids[] = $new_attr->id;
        }
      }
      if (!empty($attr_ids)) {
        $product->attrs()->sync($attr_ids);
      }
    }
    if (empty($attrs[0])) {
      $product->attrs()->sync([]);
    }
    //dd('store', $request, 'product', $product);
    $product->save();
    return back();
  }

  public function destroy(Product $product)
  {
    $product->delete();
    $product->attrs()->sync([]);
    $product->categories()->sync([]);
    return back();
  }
}
