<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller as Controller;
use App\User;

class UserSettingsController extends Controller
{
  public function index() 
  {
    $user = User::first();
    return view('admin.settings', compact('user'));
  }

  public function updateUser() 
  {
    $this->validate(request(), [
      'username' => 'required',
      'email' => [
        'required', 
        'email', 
        Rule::unique('users')->ignore(auth()->id())->where('active', 1)
      ]
    ]);
    return back();
  }
}