<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Attribute;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public $breadcrumb;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function getBreadcrumb($category) {
        if  ($category->parent != 0) {
            $this->breadcrumb[$category->id] = $category->title;
            $category = Category::where('id', $category->parent)->first();
            $this->getBreadcrumb($category);
        } else {
            $this->breadcrumb[$category->id] = $category->title;
        }
    }


    /**
     * Display the specified resource. Need to check with community whether this is the most efficient means of doing this.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
      //$products = $category->products()->with('attrs')->paginate(6);
      $categories = Category::where('parent', $category->id)->orwhere('id', $category->id)->get();     
      $this->getBreadcrumb($category);
      $breadcrumb = array_reverse($this->breadcrumb, true);
      foreach($categories as $category) {
        $parent = $category->parent;
        if ($category->parent == 0) {
            $parent = $category->id;
        } 
        $products = $category->products()->with('attrs')->get();
        if (!empty($products)) {
            foreach($products as $product) {
                $product_ids[] = $product->id;
            }
        }
      }
      if (!empty($product_ids)) {
        $products = Product::whereIn('id', $product_ids)->paginate(6);
      }
      return view('product.category', compact('products', 'parent', 'breadcrumb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
