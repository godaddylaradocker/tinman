<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public function getItems($strIndex = 'Main Menu')
    {
        $objMenu = Menu::where('title', $strIndex)->first();
        if (empty($objMenu->data)) {
           return array();
        }

        return $this->buildNavFromJSON($objMenu->data);
    }

    private function buildNavFromJSON($strJSON) 
    {  
        $objData = json_decode($strJSON);
        $arrNav = array();
        foreach($objData as $objItem){
            if($objItem->tier<1){
                $arrNav[] = $objItem;
            }
            if($objItem->tier == 1){
                $lastRootIndex = empty($arrNav) ? 0 : count($arrNav)-1;
                if(empty($arrNav[$lastRootIndex]->children)){
                    $arrNav[$lastRootIndex]->children = array();
                }
                $arrNav[$lastRootIndex]->children[]=$objItem;
            }
            if($objItem->tier == 2){
                $lastRootIndex = empty($arrNav) ? 0 : count($arrNav)-1;
                $lastChildIndex = empty($arrNav[$lastRootIndex]->children) ? 0 : count($arrNav[$lastRootIndex]->children)-1;
                
                if(empty($arrNav[$lastRootIndex]->children[$lastChildIndex]->children)){
                    $arrNav[$lastRootIndex]->children[$lastChildIndex]->children = array();
                }
                $arrNav[$lastRootIndex]->children[$lastChildIndex]->children[]=$objItem;
            }
        }
        return $arrNav;       
    }
}
