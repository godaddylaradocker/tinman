<?php

namespace App;

use App\Attr;
use App\Category;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function categories() 
    {
        return $this->belongsToMany(Category::class)->withTimestamps();
    }

    public function attrs() 
    {
        return $this->belongsToMany('App\Attr', 'attr_product')->withTimestamps();
    }

    public function listAttrs() {
        $attributes = $this->attrs()->get();
        if ($attributes->isNotEmpty()) {
            foreach($attributes as $attr) {
                $attrs[] = $attr->name;
            }
            return implode(',', $attrs);
        } else {
            return '';
        }
    }

    public function getCategory() {
        $category = $this->categories()->first();
        if (!empty($category->id)) {
            return $category->id;
        } else {
            return null;
        }
    }
}
