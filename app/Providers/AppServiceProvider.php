<?php

namespace App\Providers;

use App\Menu;
use App\Category;
use Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\UrlGenerator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
        if(env('REDIRECT_HTTPS'))
        {
            $url->forceScheme('https');
        }

        \View::composer('*', function ($view) {
            $category = \Cache::rememberForever('category', function () {
                return Category::all();
            });
            $view->with('category', $category);
        });
        \View::composer('*', function ($view) {
            $top_category = \Cache::rememberForever('top_category', function () {
                return Category::where('parent', 0)->get();
            });
            $view->with('top_category', $top_category);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
