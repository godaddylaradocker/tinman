@extends('layouts.app')

@section('content')
<div class="container">
  <div class="jumbotron">
    <h1 class="display-4">Tinman Supplies</h1>
    <p class="lead">The ONLY supplier and manufacturer of Spiral Ductwork in Teesside.</p>
    <hr class="my-4">
    <p>The members of the Tinman Supplies Team have over 30 years’ experience in the ventilation industry.  The majority of our trade counter customers are people embarking on projects that require our expert knowledge to get them moving in the right direction.</p>
    <p>If you require ventilation products or simply want to discuss your ventilation requirements then click the button below to speak to a member of our team.
    <p class="lead">
      <a class="btn btn-primary btn-lg" href="tel:01642065047" role="button">Speak to a ventilation expert</a>
    </p>
  </div>
</div>
@endsection