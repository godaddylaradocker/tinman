@extends('layouts.admin')

@section('content')
<div class="container">
<div class="row justify-content-center">
    <h1 class="pb-2">PRODUCT</h1>
</div>

  <div class="row mt-2 justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">Create Product</div>
        <div class="card-body">
          <img src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_163d5aaa1a1%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_163d5aaa1a1%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274.4296875%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="..." class="img-thumbnail">
          <form method="POST" action="{!! route('product.store') !!}" enctype="multipart/form-data">
            @csrf
            <input type="file" name="productImage"></input>
            <div class="form-group">
              <label for="titleInput">Product Title</label>
              <input type="text" id="titleInput" name="title" class="form-control" placeholder="eg. New Product" />
            </div>
            <div class="form-group">
              <label for="categoryInput">Category</label>
              <select name="category" class="form-control" id="categoryInput">
                <option value=""></option>
                @foreach ($category as $item)
                <option value="{{ $item->id }}">{{ ucwords($item->title) }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="attrInput">Attributes (comma separated)</label>
              <input type="text" id="attrInput" name="attrs" class="form-control" placeholder="eg. 250mm, 500mm" />
            </div>
            <div class="form-group">
              <label for="firstBulletInput">Descriptor 1</label>
              <input type="text" id="firstBulletInput" name="description_1" class="form-control" placeholder="eg. Gripple wire hanging systems" />
            </div>
            <div class="form-group">
              <label for="secondBulletInput">Descriptor 2</label>
              <input type="text" id="secondBulletInput" name="description_2" class="form-control" placeholder="eg. Gripple wire hanging systems" />
            </div>
            <div class="form-group">
              <label for="thirdBulletInput">Descriptor 3</label>
              <input type="text" id="thirdBulletInput" name="description_3" class="form-control" placeholder="eg. Gripple wire hanging systems" />
            </div>
            <button class="btn btn-info" type="submit">Save Product</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection