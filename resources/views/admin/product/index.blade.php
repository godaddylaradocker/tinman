@extends('layouts.admin')

@section('content')
<div class="container">
<div class="row justify-content-center">
    <h1 class="pb-2">PRODUCT</h1>
</div>
<div class="row justify-content-center pb-2">
  <form method="GET">
    <div class="form-row align-items-center">
      <div class="col-auto my-1">
        <label class="sr-only" for="inlineFormInputSearch">Search</label>
        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></div>
          </div>
          <input type="text" name="q" class="form-control" id="inlineFormInputSearch" placeholder="Search...">
        </div>
      </div>
      <div class="col-auto my-1">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </form>
</div>
<div class="row justify-content-center">
    {{ $products->links() }}
</div>
@if (request()->has('q') && count($products) == 0)
<div class="row justify-content-center">
    <p class="p-2 text-center">Sorry, we couldn't find any products matching that description. Please try a different search term or contact us.</p>
</div>
@endif
<div class="row justify-content-center pb-2">
  <form method="POST" action="{!! route('product.create') !!}">
    @csrf
    @method('GET')
    <button type="submit" class="btn btn-info">Add a new product</button>
  </form>
</div>
@foreach ($products as $product)
  <div class="row mt-2 justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">{{ $product->title }}</div>
        <div class="card-body">
          <div class="float-right">
            <form method="POST" action="{!! route('product.destroy', $product->id) !!}">
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-danger">Delete</button>
            </form>
          </div>
          @if ($product->image)
            <img height="200" width="200" class="img-thumbnail" src="/storage/products{{ $product->image }}" />
          @else
            <img src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_163d5aaa1a1%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_163d5aaa1a1%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274.4296875%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="..." class="img-thumbnail">
          @endif
          <form method="POST" action="{!! route('product.update', $product->id) !!}" enctype="multipart/form-data">
            @csrf
            @csrf
            @method('PATCH')
            <input type="file" name="productImage"></input>
            <div class="form-group">
              <label for="titleInput">Product Title</label>
              <input type="text" id="titleInput" name="title" class="form-control" placeholder="eg. New Product" value="{{ $product->title or '' }}">
            </div>
            <div class="form-group">
              <label for="categoryInput">Category</label>
              <select name="category" class="form-control" id="categoryInput">
                <option value=""></option>
                @foreach ($category as $item)
                @if($item->id == $product->getCategory())
                <option value="{{ $item->id }}" selected>{{ ucwords($item->title) }}</option>
                @else
                <option value="{{ $item->id }}">{{ ucwords($item->title) }}</option>
                @endif
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="attrInput">Attributes (comma separated)</label>
              <input type="text" id="attrInput" name="attrs" class="form-control" placeholder="eg. 250mm, 500mm" value="{{ $product->listAttrs() }}">
            </div>
            <div class="form-group">
              <label for="firstBulletInput">Descriptor 1</label>
              <input type="text" id="firstBulletInput" name="description_1" class="form-control" placeholder="eg. Gripple wire hanging systems" value="{{ $product->description_1 or '' }}">
            </div>
            <div class="form-group">
              <label for="secondBulletInput">Descriptor 2</label>
              <input type="text" id="secondBulletInput" name="description_2" class="form-control" placeholder="eg. Gripple wire hanging systems" value="{{ $product->description_2 or '' }}">
            </div>
            <div class="form-group">
              <label for="thirdBulletInput">Descriptor 3</label>
              <input type="text" id="thirdBulletInput" name="description_3" class="form-control" placeholder="eg. Gripple wire hanging systems" value="{{ $product->description_3 or '' }}">
            </div>
            <button class="btn btn-info" type="submit">Save Product</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endforeach
  <div class="row justify-content-center pt-2">
    {{ $products->links() }}
  </div>
</div>
@endsection