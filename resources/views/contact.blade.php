@extends('layouts.app')

@section('content')
<div class="container">
  <div class="card card-group" style="width: 18rem;">
    <div class="card-body">
      <h5 class="card-title">Where to find us...</h5>
      <h6 class="card-subtitle mb-2 text-muted">Tinman Supplies Ltd.,</h6>
      <p class="card-text" style="text-align: left;">
        Infinity House,<br>
        Skippers Lane Ind Est,<br>
        Infinity House,<br>
        Sotherby Road,<br>
        Middlesbrough,<br>
        TS3 8BT<br>
        Tel. 01642 065047<br>
        <a href="mailto:sales@tinmansupplies.co.uk" class="card-link">sales@tinmansupplies.co.uk</a><br>
        <a href="/" class="card-link">www.tinmansupplies.co.uk</a>
      </p>
    </div>
  </div>
</div>
<div class="container">
  <div id="home-map" class="bg-white w-100 shadow p-3 mb-5 border border-light d-block"></div>
</div>
<div class="container">
  <div class="card-group">
    <div class="card">
      <img class="card-img-top" src="/storage/person-placeholder.png" alt="Card image cap">
      <div class="card-body">
        <h5 class="card-title">Trade Counter</h5>
        <p>Tinman Supplies Ltd., <br> Infinity House, <br> Skippers Lane Ind Est, <br> Sotherby Road, <br> Middlesbrough, <br> TS3 8BT</p>
        <a href="tel:01642065047" class="btn btn-primary">Call</a>
      </div>
    </div>
    <div class="card">
      <img class="card-img-top" src="/storage/person-placeholder.png" alt="Card image cap">
      <div class="card-body">
        <h5 class="card-title">Mark Steel</h5>
        <p>Sales Manager</p>
        <a href="mailto:marksteel@tinmansupplies.co.uk" class="btn btn-primary">Email</a>
      </div>
    </div>
    <div class="card">
      <img class="card-img-top" src="/storage/person-placeholder.png" alt="Card image cap">
      <div class="card-body">
        <h5 class="card-title">Sales</h5>
        <p>Sales Administrator</p>
        <a href="mailto:sales@tinmansupplies.co.uk" class="btn btn-primary">Email</a>
      </div>
    </div>
  </div>
</div>
<script>
  function initMap() {
    let 
    styled = [{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#000000"},{"saturation":36},{"lightness":40}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#000000"},{"lightness":16},{"visibility":"on"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"lightness":17},{"weight":1.2}]},{"featureType":"administrative","elementType":"labels.text","stylers":[{"color":"#feec0c"},{"visibility":"simplified"}]},{"featureType":"administrative.country","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"visibility":"off"}]},{"featureType":"administrative.country","elementType":"labels.text","stylers":[{"color":"#ffc400"},{"weight":"0.26"}]},{"featureType":"administrative.locality","elementType":"geometry","stylers":[{"color":"#f9ed32"}]},{"featureType":"administrative.locality","elementType":"geometry.fill","stylers":[{"color":"#ff0000"}]},{"featureType":"administrative.locality","elementType":"labels.text","stylers":[{"color":"#ffc400"},{"lightness":"0"},{"visibility":"on"},{"weight":"0.83"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text","stylers":[{"hue":"#fff000"},{"visibility":"off"}]},{"featureType":"administrative.province","elementType":"geometry","stylers":[{"color":"#f9ed32"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#1b1a1a"},{"lightness":20}]},{"featureType":"landscape","elementType":"labels.text","stylers":[{"color":"#feec0c"},{"visibility":"simplified"}]},{"featureType":"landscape.man_made","elementType":"labels.text","stylers":[{"color":"#feec0c"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"poi","elementType":"labels.text","stylers":[{"color":"#feec0c"},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels.text","stylers":[{"color":"#feec0c"},{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"color":"#000000"},{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"color":"#feec0c"},{"weight":0.5}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#212121"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#ffc400"},{"lightness":17}]}],
    myLatLng = {lat: 54.570766, lng: -1.196622},
    map = new google.maps.Map(document.getElementById("home-map"), {
        center: myLatLng,
        scrollwheel: false,
        styles: styled,
        disableDefaultUI: true,
        zoom: 17
    }),
    marker = new google.maps.Marker({
        map: map,
        position: myLatLng,
        title: 'Tinman'
    });
  }
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACikvO3LZ0SN5NFG5fzIspnbrM8n9Wtl0&callback=initMap"></script>
<style>
#home-map {
    height: 400px;
}
.card-group {
  margin-bottom: 2rem;
}
</style>
@endsection
