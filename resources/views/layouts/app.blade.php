<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="description" content="Tinman Supplies are here to offer a friendly and trustworthy service. We can provide a wide range of ventilation products from galvanised steel ductwork to PVC ductwork alongside various ancillaries and more. We are Teesside’s only dedicated manufacturer and supplier, with over 30 years of experience in the industry. We can also offer a range of H-vac insulation products that will be suitable for most projects."/>

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>
  <script src="https://use.fontawesome.com/webfontloader/1.6.24/webfontloader.js"></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Palanquin+Dark:700" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/d47cac325e.css" media="all">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="application">
        <div class="container-fluid bg-secondary text-white text-center">
            <button type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <span>We use cookies to give you the best possible experience on our site. By continuing to use the site you agree to our use of cookies. <a class="text-white" href=""><u>Find out more</u></a>.<span>
        </div>

        <div class="container-fluid bg-primary" style="background-image: linear-gradient(to top,#ffed00,#d2c304);">
            <div class="row d-flex align-items-center p-2">
                <div class="col text-center align-middle">
                    <a href="tel:01642065047" class="nounderline" class="font-weight-bold">Call us on: 01642 065047</a>
                </div>
                <div class="col text-center d-none d-lg-block" style="display: table-cell; vertical-align: middle;">
                    <span class="font-weight-bold"><a class="nounderline" href="mailto:sales@tinman.co.uk">Email: sales@tinmansupplies.co.uk</a></span>
                </div>
                <div class="col text-center d-none d-lg-block">
                    <ul class="social-links list-unstyled" style="display: flex;justify-content: center;margin: 0;">
                        <li class="linkedin"><button class="soc-border"><i class="fa fa-linkedin" aria-hidden="true"></i></button></li>
                        <li class="twitter"><button class="soc-border"><i class="fa fa-twitter" aria-hidden="true"></i></button></li>
                        <li class="facebook"><button class="soc-border"><i class="fa fa-facebook" aria-hidden="true"></i></button></li>
                    </ul>
                </div>
            </div>
        </div>

        <nav class="navbar navbar-expand-lg navbar-light navbar-laravel">
            <div class="container">
                <div id="logo">
                    <a href="/"><img height="50px" src="/storage/logo.png"></a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>


                    <!-- Right Side Of Navbar -->
                    <ul class="pt-2 navbar-nav ml-auto">
                            <!-- <li>
                                <div class="row justify-content-center">
                                    <form method="GET">
                                        <div class="form-row align-items-center mr-3">
                                            <div class="col-auto my-1">
                                                <label class="sr-only" for="inlineFormInputSearch">Search</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></div>
                                                    </div>
                                                    <input type="text" name="q" class="form-control" id="inlineFormInputSearch" placeholder="Search...">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </li> -->
                        <!-- Category Links -->
                        @foreach ($top_category as $item)
                        <li class="d-block d-lg-none"><a class="font-weight-bold nav-link" href="/category/{{ $item->id }}">{{ $item->title }}</a></li>
                        @endforeach
                        <!-- Authentication Links -->
                        <li><a class="nav-link" href="/">{{ __('Homepage') }}</a></li>
                        <li><a class="nav-link" href="{{ route('about') }}">{{ __('About Us') }}</a></li>
                        @guest
                            <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                            <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                        <li><a class="nav-link" href="{{ route('contact') }}">{{ __('Contact Us') }}</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel d-none d-lg-block border-top border-light">
            <div class="container">
                <ul class="d-table pl-0">
                    @foreach ($top_category as $item)
                    <li class="d-table-cell text-center align-middle nav-item">
                        <small class="font-weight-bold"><a class="nav-link text-uppercase" href="/category/{{ $item->id }}">{{ $item->title }}</a></small>
                    </li>
                    @endforeach
                </ul>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>

        <footer>
            <div class="container-fluid bg-primary">
                <div class="container pb-4 pt-5">
                    <div class="row">
                        <div class="col-8">
                            <a href="/"> 
                                <img class="mb-2" height="30px" src="/storage/logo.png" />
                            </a>
                            <p>© Copyright Tinman Supplies Ltd 2017, <br> Company Registration no: 10641732 VAT Registered: 268683941 2018.</p>
                        </div>
                        <div class="col-4">
                            <ul class="social-links list-unstyled float-right">
                                <li class="linkedin"><button class="soc-border"><i class="fa fa-linkedin" aria-hidden="true"></i></button></li>
                                <li class="twitter"><button class="soc-border"><i class="fa fa-twitter" aria-hidden="true"></i></button></li>
                                <li class="facebook"><button class="soc-border"><i class="fa fa-facebook" aria-hidden="true"></i></button></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

<style>
/*Social Meds */
.instagram .twitter .linkedin {
  width: 50px;
  height: 50px;
}
.nounderline {
  text-decoration: none !important
}
.soc-border {
  background: transparent;
  outline: none;
  color: #000;
  width: 30px;
  height: 30px;
  -webkit-border-radius: 100px;
  -moz-border-radius: 100px;
  border-radius: 100px;
  border: 2px solid #000;
  transition: all .2s ease-in-out;
  margin: 5px;
}
.linkedin .soc-border:hover {
  background: #517fa4;
  border-color: #7495af;
  color: rgba(255,255,255, 0.9);
  transform: scale(1.2);
}
.facebook .soc-border:hover {
  background: #3b5998;
  border-color: #436aaf;
  color: rgba(255,255,255, 0.9);
  transform: scale(1.2);
}
.twitter .soc-border:hover {
  background: #00aced;
  border-color: #72c6e6;
  color: rgba(255,255,255, 0.9);
  transform: scale(1.2);
}
i.fa.fa-twitter {
  font-size: 16px;
}
i.fa.fa-facebook {
  font-size: 16px;
}
i.fa.fa-linkedin {
  font-size: 16px;
}
ul.social-links > li > button {
    cursor: pointer;
}
</style>
    </div>
</body>
</html>
