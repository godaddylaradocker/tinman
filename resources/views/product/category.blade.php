@extends('layouts.app')

@section('content')
<div class="container">

  @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
  @endif

  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item" aria-current="page"><a href="/">Home</a></li>
      @foreach ($breadcrumb as $id =>$crumb)
      <li class="breadcrumb-item {{ $loop->last ? 'active' : ''}}" aria-current="page"><a href="/category/{{ $id }}">{{ $crumb }}</a></li>
      @endforeach
    </ol>
  </nav>

  {{ $products->links() }}

  <div class="row">
    <div class="d-none d-md-block col-md-4 col-lg-3">
      <div class="card">
        <div class="card-header bg-dark text-white">
          <p class="d-inline-block m-0 align-middle">Categories</p>
          <img class="d-inline-block box-image float-right" src="/storage/box.png" />
        </div>
        <div class="card-body">
          <div class="product-tab-lower">
            <div class="product-menu">
              <ul id="menu-items">
                @foreach ($top_category as $item)
                    @if ($item->id == $parent)
                    <li class="align-middle nav-item menu-item-has-children">
                      <a class="nav-link" href="/category/{{ $item->id }}">{{ $item->title }}</a>
                      <ul class="sub-menu">
                      @foreach ($item->related() as $related)
                        <li class="align-middle nav-item">
                          <a class="nav-link" href="/category/{{ $related->id }}">{{ $related->title }}</a>
                        </li>
                      @endforeach
                      </ul>
                    </li>
                    @else
                    <li class="align-middle nav-item">
                      <a class="nav-link" href="/category/{{ $item->id }}">{{ $item->title }}</a>
                    </li>
                    @endif
                @endforeach
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-8 col-lg-9">
      <div class="row">
        @foreach($products as $product)
        <div class="col-md-12 col-lg-6 row-eq-height">
          <div class="col d-flex flex-column bg-white product-card">
            <div class="row">
              <div class="col-sm-12">
                <h3>{{ $product->title }}</h3>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-5">
                @if (empty($product->image))
                  <img src="/storage/awaiting_image.jpg" alt="..." class="rounded thumb-sm">
                @else
                  <img height="136" width="136" src="/storage/products{{ $product->image }}" alt="..." class="rounded thumb-sm">
                @endif
              </div>
              <div class="col-sm-7">
                <form>
                  @if ($product->attrs->count())
                  <p>Please select the size you require:</p>
                  <select class="form-control form-control-sm">
                    @foreach($product->attrs as $attr)
                    <option>{{ $attr->name }}</option>
                    @endforeach
                  </select>
                  @endif
                </form>
                <ul style="padding:20px;">
                @if (!empty($product->description_1))
                  <li>{{ $product->description_1 }}</li>
                @endif
                @if (!empty($product->description_2))
                  <li>{{ $product->description_2 }}</li>
                @endif
                </ul>
              </div>
            </div>
            <div class="row mt-auto">
              <div class="col-sm-12">
                <h1 class="border-top border-primary text-right m-2">£ Enquire Only</h1>
              </div>
            </div>
            <div class="row mt-3">
              <div class="col-sm-6">
                <a href="/product/notify/{{ $product->id }}">
                  <button type="button" class="btn btn-primary col">Request a Callback</button>
                </a>
              </div>
              <div class="col-sm-6">
                <a href="mailto:sales@tinmansupplies.co.uk?subject={{ $product->title }}">
                  <button type="button" class="btn btn-secondary col">Email Us</button>
                </a>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
@endsection
