@extends('layouts.app')

@section('content')
  @foreach($products as $product)
    <li class="{{ $loop->last ? 'last' : '' }}">{{ $product->title }}</li>
  @endforeach

  {{ $products->links() }}
@endsection