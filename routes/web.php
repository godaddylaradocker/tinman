<?php

// use App\Mail\ProductEnquiry;
// use App\User;
// use App\Product;
// use App\Category;
// use App\Notifications\ProductEnquiryNotification;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('homepage');
});

Route::get('/about', function () {
  return view('about');
})->name('about');

Route::get('/contact', function () {
  return view('contact');
})->name('contact');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/product/notify/{product}', 'ProductController@notify')->middleware('auth');

Route::get('/category/{category}', 'CategoryController@show');

Auth::routes();


// Route::get('/mail', function() {
//   $productenquiryemail = new ProductEnquiry(new User(['name' => 'Matthew']));
//   Mail::to('matthew.anderson@mmediadesign.co.uk')->send($productenquiryemail);
// });

// Route::get('/notify', function() {
//   $user = App\User::first();
//   $product = App\Product::first();
//   //dd($user, $product);
//   $user->notify(new ProductEnquiryNotification($product));
// });

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes within The "App\Http\Controllers\Admin" namespace.
|
*/
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {
  Route::resource('/category', 'CategoryController');
  Route::resource('/product', 'ProductController');

  Route::get('/settings/account', 'UserSettingsController@index')->name('account_settings');
  Route::patch('/settings/account/update', 'UserSettingsController@updateUser')->name('account_settings_update');

});
